<?php
//class Hewan
abstract class Hewan{
    protected $nama;
    protected $darah = 50;
    protected $jumlah_kaki;
    protected $keahliahn;

    public function __construct($nama) {
        $this->nama = $nama;
      }

    public function atraksi(){
        return $this->nama . ' sedang ' . $this->keahlian;
    }
  
}
// class fight
abstract class Fight extends Hewan{
    protected $attackPower;
    protected $defencePower;
    public function serang($other){
        echo 'Diserang : ' .$other->diserang($this) .'<br>';
        return $this->nama . ' sedang menyerang ' . $other->nama;
    }
    public function diserang($other){
        $this->darah = $this->darah - $other->attackPower / $this->defencePower;
        return $this->nama . ' sedang diserang ' . $other->nama ;
    }
}

//class Elang
class Elang extends Fight{
    protected $jumlah_kaki = 2;
    protected $keahlian = "terbang tinggi";
    public $attackPower = 10;
    public $defencePower = 5;
    private $jenis = "Elang";

    public function getInfoHewan(){
        return 'Nama Hewan : ' . $this->nama.'<br>'. 'Jenis Hewan : '. $this->jenis . '<br>' .
            'Darah : ' . $this->darah . '<br>'.'Jumlah Kaki :'. $this->jumlah_kaki. '<br>'. 
                'Keahlian : '.$this->keahlian. '<br>'.'Attack Power :'. $this->attackPower ."<br>" .
                'Defence Power : ' . $this->defencePower .'<br>';
    }
}
//class Harimau
class Harimau extends Fight {
    public $jumlah_kaki = 4;
    public $keahlian = "lari cepat";
    public $attackPower = 7;
    public $defencePower = 8;
    private $jenis = "Harimau";
    
    public function getInfoHewan(){
        return 'Nama Hewan : ' . $this->nama.'<br>'. 'Jenis Hewan : '. $this->jenis . '<br>' .
               'Darah : ' . $this->darah . '<br>'.'Jumlah Kaki :'. $this->jumlah_kaki. '<br>'. 
                'Keahlian : '.$this->keahlian. '<br>'.'Attack Power :'. $this->attackPower ."<br>" .
                'Defence Power : ' . $this->defencePower .'<br>';
    }
}


echo "<h1>Tugas Pendalaman OOP PHP</h1>";
$elang = new Elang("elang_1");
echo $elang->getInfoHewan() ;
$kelas = get_class($elang);
echo 'Jenis Kelas : ' . $kelas . '<br><br>';

$harimau = new Harimau('Harimau_1');
echo 'Atraksi : ' .$elang->atraksi() . '<br>';
echo 'Serang : ' . $elang->serang($harimau). '<br><br>';
echo $harimau->getInfoHewan() . '<br>';
?>
